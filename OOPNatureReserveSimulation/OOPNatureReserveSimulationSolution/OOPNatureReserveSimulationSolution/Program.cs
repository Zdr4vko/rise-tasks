﻿using OOPNatureReserveSimulationSolution.Animals;

namespace OOPNatureReserveSimulationSolution;

public class Program
{
    public static void Main(string[] args)
    {
        var (minLifeSpan, maxLifeSpan, avgLifeSpan) = AnimalLifeSpan.SimulateLifeSpan();
        Console.WriteLine(
            $@"
The minimum lifespan of the animals is {minLifeSpan} days,
the average is {avgLifeSpan} days
and the maximum is {maxLifeSpan} days."
        );
        var animal = new Bear();
    }
}