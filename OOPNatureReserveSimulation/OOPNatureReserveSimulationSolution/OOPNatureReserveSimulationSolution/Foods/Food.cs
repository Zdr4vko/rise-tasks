﻿namespace OOPNatureReserveSimulationSolution.Foods;

public abstract class Food : IFood
{
    private string Name { get; }
    public int NutritionalValue { get; set; }
    public bool IsAlive { get; set; } = true;

    protected Food(string name, int nutritionalValue)
    {
        Name = name;
        NutritionalValue = nutritionalValue;
    }

    public int ConsumeNutrition(int nutrition)
    {
        if (nutrition > NutritionalValue)
        {
            var result = NutritionalValue;
            NutritionalValue = 0;
            return result;
        }

        NutritionalValue -=nutrition;

        return nutrition;
    }
}