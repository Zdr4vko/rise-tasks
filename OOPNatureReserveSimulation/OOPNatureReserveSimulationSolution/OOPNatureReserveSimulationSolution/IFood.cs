﻿namespace OOPNatureReserveSimulationSolution
{
    public interface IFood
    {
        public int NutritionalValue { get; set; }
        int ConsumeNutrition(int maximumEnergy);
        public bool IsAlive { get; set; }
    }
}
