﻿namespace OOPNatureReserveSimulationSolution.Animals;

public abstract class Animal : Food, IAnimal
{
    private int MaximumEnergy { get; }
    private int CurrentEnergy { get; set; }
    private HashSet<string> Diet { get; }
    private string AnimalType { get; }
    private int AnimalAge { get; set; }

    protected Animal(string name, int nutritionalValue, int maximumEnergy, HashSet<string> diet, string animalType) : base(name, nutritionalValue)
    {
        MaximumEnergy = maximumEnergy;
        CurrentEnergy = maximumEnergy;
        Diet = diet;
        AnimalType = animalType;
    }

    public void Feed(IFood foodItem, List<Animal> listOfAnimals)
    {
        if (FoodItemInDiet(foodItem) && foodItem.NutritionalValue > 0 && foodItem.IsAlive)
        {
            if (CurrentEnergy < MaximumEnergy)
            {
                CurrentEnergy += foodItem.ConsumeNutrition(MaximumEnergy - CurrentEnergy);

                if (FoodIsAnimal(foodItem))
                {
                    GetEaten(foodItem);
                }

                AnimalSuccessfullyFed(foodItem);
                Age();
            }
        }
        else
        {
            CurrentEnergy--;

            if (CurrentEnergy < 1)
            {
                Dies(foodItem, listOfAnimals);
            }
            else
            {
                if (CurrentEnergy < 6)
                {
                    Starves(foodItem);
                    Age();
                }
                else
                {
                    LoseEnergy(foodItem);
                    Age();
                }
            }
        }
    }

    public void GetEaten(IFood foodItem)
    {
        foodItem.IsAlive = false;
    }

    private bool FoodItemInDiet(IFood foodItem)
    {
        return Diet.Contains(foodItem.GetType().Name);
    }

    private void AnimalSuccessfullyFed(IFood foodItem)
    {
        Console.WriteLine($"{GetType().Name} was fed with {foodItem.GetType().Name}.");
    }

    public void Starves(IFood foodItem)
    {
        Console.Beep();
        Console.WriteLine($"A {GetType().Name} couldn't eat {foodItem.GetType().Name} and is beginning to starve.");
    }

    public void Dies(IFood foodItem, List<Animal> listOfAnimals)
    {
        IsAlive = false;
        listOfAnimals.Remove(this);

        Console.WriteLine($"A {GetType().Name} couldn't eat {foodItem.GetType().Name} and died.");
    }

    private void LoseEnergy(IFood foodItem)
    {
        Console.WriteLine($"A {GetType().Name} couldn't eat {foodItem.GetType().Name} and lost some energy.");
    }

    private bool FoodIsAnimal(IFood foodItem)
    {
        return foodItem.GetType().BaseType == typeof(Animal);
    }

    public int[] UpdateStatistics(int turns, int minLifeSpanOfAnimals, int maxLifeSpanOfAnimals, int averageLifeSpanOfAnimals, int[] currentStatistics)
    {
        if (!IsAlive)
        {
            if (minLifeSpanOfAnimals > turns)
            {
                minLifeSpanOfAnimals = turns;
            }

            if (maxLifeSpanOfAnimals < turns)
            {
                maxLifeSpanOfAnimals = turns;
            }

            averageLifeSpanOfAnimals += turns;
        }

        currentStatistics[0] = minLifeSpanOfAnimals;
        currentStatistics[1] = averageLifeSpanOfAnimals;
        currentStatistics[2] = maxLifeSpanOfAnimals;

        return currentStatistics;
    }

    private void Age()
    {
        this.AnimalAge++;
    }
}