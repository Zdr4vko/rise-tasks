﻿namespace OOPNatureReserveSimulationSolution.Animals;

public class Wolf : Animal
{
    public Wolf() : base("wolf", 7, 15, new HashSet<string> { "Beaver", "Hare", "Rat", "Pigeon" }, "Carnivore")
    {
    }
}