﻿namespace OOPNatureReserveSimulationSolution
{
    interface IPlant : IFood
    {
        void RegenerateNutrients();
        void GetEaten();
    }
}
