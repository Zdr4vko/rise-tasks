﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_DataStructures
{
    public class UniqueElements
    {
        public List<int> findUniqueElementsInList (List<int> list)
        {

            if (list.Count == 0)
            {
                return list;
            }

            HashSet<int> uniqueElementsInList = new HashSet<int>(list);

            return uniqueElementsInList.ToList();
        }
    }
}
