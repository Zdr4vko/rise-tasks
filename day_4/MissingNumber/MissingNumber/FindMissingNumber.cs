﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MissingNumber
{
    public class FindMissingNumber
    {
        public static int FindMissingNumberFn(int[] array)
        {
            int number = 1;
            bool arrIsSorted = true;
            int saveNum;

            if (array.Length < 2)
            {
                throw new ArgumentException("The input array must contain atleast 2 numbers");
            }

            if (!ContainsOnlyPositiveIntegers(array))
            {
                throw new ArgumentException("There are negative numbers in the array");
            }

            while (true)
            {

                for (int i = 0; i < array.Length - 1; i++)
                {

                    if (array[i] > array[i + 1])
                    {
                        arrIsSorted = false;
                        break;
                    }

                    else
                    {
                        arrIsSorted = true;
                    }
                }

                if (arrIsSorted)
                {
                    break;
                }

                for (int i = 0; i < array.Length - 1; i++)
                {

                    if (array[i] > array[i + 1])
                    {
                        saveNum = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = saveNum;
                    }
                }
                
            }

            for (int i = 0; i < array.Length - 1; i++)
            {
                int first = array[i];
                int second = array[i + 1];

                if (second - first != 1)
                {
                    number = second - 1;
                    return number;
                }
            }

            return number;
        }

        public static bool ContainsOnlyPositiveIntegers(int[] arr)
        {
            foreach (int i in arr)
            {

                if (i < 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
