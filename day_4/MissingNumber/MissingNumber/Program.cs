﻿namespace MissingNumber
{
    public class Program
    {
        public static void Main()
        {
            int[] arr = { 3, 1, 4 };

            Console.WriteLine(FindMissingNumber.FindMissingNumberFn(arr));
            Console.ReadLine();
        }
    }
}