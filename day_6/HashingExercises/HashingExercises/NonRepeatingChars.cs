﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashingExercises
{
    public class NonRepeatingChars
    {
        public static HashSet<char> findNonRepeatingCharsInAString (string str)
        {
            char[] charsInString = str.ToCharArray();

            var uniqueChars = charsInString.GroupBy(x => x).Where(x => x.Count() == 1);

            HashSet<char> nonRepeatingChars = new HashSet<char>();

            foreach (var item in uniqueChars)
            {
                nonRepeatingChars.Add(item.FirstOrDefault());
            }

            return nonRepeatingChars;
        }
    }
}
