﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashingExercises
{
    public class OwnersWithMoreThanOneCar
    {
        public static void FindOwnersWithMoreThanOneCar (Dictionary<string, string> carRegistry)
        {
            var duplicateValues = carRegistry.GroupBy(x => x.Value).Where(x => x.Count() > 1);

            foreach (var item in duplicateValues)
            {
                Console.WriteLine(item.Key);
            }
        }
    }
}
