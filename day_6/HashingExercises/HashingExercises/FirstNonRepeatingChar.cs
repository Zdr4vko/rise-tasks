﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashingExercises
{
    public class FirstNonRepeatingChar
    {
        public static char findFirstNonRepeatingCharInStr (string str)
        {

            foreach (char i in str)
            {

                if (str.Count(j => j == i) == 1)
                {
                    return i;
                }
            }

            return '1';
        }
    }
}
