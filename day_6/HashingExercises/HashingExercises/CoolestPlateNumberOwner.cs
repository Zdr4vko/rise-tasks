﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HashingExercises
{
    public class CoolestPlateNumberOwner
    {
        public static List<string> CoolestPlateOwner(Dictionary<string, string> carRegistry)
        {
            List<string> coolestPlateNumbersInCarRegistry = new List<string>();

            foreach (string carRegNumber in carRegistry.Keys)
            {
                String resultString;
                resultString = Regex.Match(carRegNumber, @"\d+").Value;

                if ((resultString.Distinct().Count() == 1))
                {
                    coolestPlateNumbersInCarRegistry.Add(carRegNumber);
                }
            }

            return coolestPlateNumbersInCarRegistry;
        }
    }
}
