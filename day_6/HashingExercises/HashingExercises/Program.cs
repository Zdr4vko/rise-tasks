﻿using System.Collections.Generic;
using System.Numerics;
using System.Runtime.Intrinsics.X86;
using System.Text.RegularExpressions;

namespace HashingExercises
{
    public class Program
    {
        public static void Main()
        {
            Dictionary<string, string> carRegNumberToOwner = new Dictionary<string, string>();

            carRegNumberToOwner.Add("CA3809AA", "Boris Borisov");
            carRegNumberToOwner.Add("EN7272HF", "Boiko Boikov");
            carRegNumberToOwner.Add("AH1111FH", "Emil Dimitrov");
            carRegNumberToOwner.Add("CB888888", "Alex Sasho");
            carRegNumberToOwner.Add("BP8971SD", "Boris Borisov");
            carRegNumberToOwner.Add("K7999JK", "Alex Sasho");
            carRegNumberToOwner.Add("MO5555JJ", "Martin Borisov");
            carRegNumberToOwner.Add("T0303OK", "Boris Borisov");

            List<string> coolestPlates = new List<string>(CoolestPlateNumberOwner.CoolestPlateOwner(carRegNumberToOwner));

            foreach (string coolPlate in coolestPlates)
            {
                Console.WriteLine(coolPlate);
            }

            OwnersWithMoreThanOneCar.FindOwnersWithMoreThanOneCar(carRegNumberToOwner);

            string[] arr1 = new string[] { "a", "b", "c" };
            string[] arr2 = new string[] { "f", "g", "a", "d", "b", "o", "r", "c" };

            string[] arrsIntersected = new string[] { };
            arrsIntersected = IntersectionOfTwoArrays.FindIntersectionOfTwoArrays(arr1, arr2);

            foreach (var item in arrsIntersected)
            {
                Console.WriteLine(item);
            }

            string str = "ooooooasavsasjjkok";

            HashSet<char> nonRepeatingCharsSet = new HashSet<char>();

            nonRepeatingCharsSet = NonRepeatingChars.findNonRepeatingCharsInAString(str);

            foreach (var item in nonRepeatingCharsSet)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("-----------------------------------");

            Console.WriteLine(FirstNonRepeatingChar.findFirstNonRepeatingCharInStr(str));

            Console.WriteLine();
            Console.ReadLine();
        }
    }
}