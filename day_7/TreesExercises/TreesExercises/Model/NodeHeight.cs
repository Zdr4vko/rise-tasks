﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreesExercises.Model
{
    public class NodeHeight
    {
        public static int FindNodeHeight (BinaryTreeNode node)
        {

            if (node == null)
            {
                return 0;
            }

            int leftHeight = FindNodeHeight(node.Left);
            int rightHeight = FindNodeHeight(node.Right);

            return Math.Max(leftHeight, rightHeight) + 1;

        }
    }
}
