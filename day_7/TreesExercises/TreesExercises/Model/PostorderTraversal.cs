﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreesExercises.Model
{
    public class PostorderTraversal
    {
        public static void PrintNodesInPostorderTraversal(BinaryTreeNode binaryTree)
        {

            if (binaryTree == null)
            {
                return;
            }

            PrintNodesInPostorderTraversal(binaryTree.Left);
            PrintNodesInPostorderTraversal(binaryTree.Right);

            Console.Write(binaryTree.Value + " ");
        }
    }
}
