﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphExercises
{
    public class Graph
    {
        private int _vertexCount;
        private List<List<int> > adjacent;

        public Graph(int count)
        {
            _vertexCount = count;
            adjacent = new List<List<int>>(_vertexCount);

            for (int i = 0; i < _vertexCount; i++)
            {
                adjacent.Add(new List<int>());
            }
        }

        public void AddEdge(int start, int end)
        {
            adjacent[start].Add(end);
        }

        public bool isCyclic()
        {
            bool[] visited = new bool[_vertexCount];
            bool[] recStack = new bool[_vertexCount];

            for (int i = 0; i < _vertexCount; i++)
                if (isCyclicUtil(i, visited, recStack))
                    return true;

            return false;
        }

        public bool isCyclicUtil(int i, bool[] visited, bool[] recStack)
        {
           
            if (recStack[i])
                return true;

            if (visited[i])
                return false;

            visited[i] = true;

            recStack[i] = true;
            List<int> children = adjacent[i];

            foreach (int c in children) if (
                isCyclicUtil(c, visited, recStack)) return true;

            recStack[i] = false;

            return false;
        }
    }
}
